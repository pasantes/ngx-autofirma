// MODULE DEPENDENCIES
// - import module dependencies

// -- angular dependencies --
import { Component        } from '@angular/core';

// -- ngx-autofirma dependencies --
import { AutofirmaService } from 'ngx-autofirma'



@Component({
  selector: 'ngx-autofirma-demo',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private autofirma: AutofirmaService) {}
}
