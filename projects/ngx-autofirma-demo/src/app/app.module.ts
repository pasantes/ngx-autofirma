// MODULE DEPENDENCIES
// - import module dependencies

// -- angular dependencies --
import { NgModule           } from '@angular/core';
import { BrowserModule      } from '@angular/platform-browser';

// -- app module dependencies --
import { AppComponent       } from './app.component';

// -- ngx-autofirma module dependencies --
import { NgxAutofirmaModule } from 'ngx-autofirma';



// MODULE DEFINITION
// - set module configuration throught annotations

/**
 * We use App Module as the starting point for our Angular application (Root Module)
 *
 * The App Module should contain singleton services, universal components
 * and other features where there's only once instance per application.
 */
@NgModule({
  bootstrap:    [ // -- components --
                  AppComponent
                ],
  declarations: [ // -- components --
                  AppComponent
                ],
  imports:      [ // -- angular modules --
                  BrowserModule,
                  // -- ngx-autofirma module --
                  NgxAutofirmaModule,
                ],
})
export class AppModule {}
