// MODULE DEPENDENCIES
// - import module dependencies

// -- angular dependencies --
import { enableProdMode         } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

// -- app module dependencies --
import { AppModule              } from './app/app.module';

// -- environment configuration --
import { environment            } from './environments/environment';



// CHECK ENVIRONMENT STAGE
// - detect if it's running on develop or production mode
if (environment.production) {
  enableProdMode();
}



// APPLICATION BOOTSTRAP
// - our application start loading and executing our Root Module (AppModule)
platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
