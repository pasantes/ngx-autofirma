/*
 * Public API Surface of ngx-autofirma
 */

export { NgxAutofirmaModule        } from './lib/ngx-autofirma.module';
export { autofirmaServiceFactory   } from './lib/services/autofirma.provider';
export { AutofirmaClient,
         AutofirmaService          } from './lib/services/autofirma.service';
export { AutofirmaWebSocketService } from './lib/services/autofirma-web-socket.service';
export { KeyStore                  } from './lib/types/keystore';
export   * as AutofirmaUtils         from './lib/utils/autofirma.utils';
export   * as Platform               from './lib/utils/platform.utils';
