// MODULE DEPENDENCIES
// - import module dependencies

// -- ngx-autofirma module dependencies --
import { AutofirmaWebSocketService } from "./autofirma-web-socket.service";



// UNIT TESTS DEFINITION
// - set of unit test for Autofirma web socket service
describe('Unit tests: Autofirma Web Socket Service', () => {
  let autofirma: AutofirmaWebSocketService;

  // - initialize test
  beforeAll(() => { autofirma = new AutofirmaWebSocketService(); }, 5 * 1000);

  // - define unit tests
  it('autofirma echo operation should return "OK"', done => {
    autofirma.echo().subscribe({
      next: echo => {
        expect(echo).toBe('OK');
        done();
      }
    });
  });
});
