// MODULE DEPENDENCIES
// - import module dependencies

// -- rxjs dependencies --
import { Observable } from 'rxjs';

// -- ngx-autofirma dependencies --
import { KeyStore   } from '../types/keystore';



export interface AutofirmaClient {
  echo(): Observable<string>;
  setKeyStore(keyStore: KeyStore | null): void;
  // sign(): void;
  // coSign(): void;
  // counterSign(): void;
  // signBatch(): void;
  // selectCertificate(): void;
  // saveDataToFile(): void;
  // signAndSaveToFile(): void;
  // getFileNameContentBase64(): void;
  // getMultiFileNameContentBase64(): void;
  setStickySignatory(sticky: boolean): void;
  // setLocale(): void;
  getErrorMessage(): string;
  getErrorType(): string;
  // getCurrentLog(): void;
}



export abstract class AutofirmaService implements AutofirmaClient {
  protected keyStore: KeyStore | null = null;
  protected stickySignatory: boolean = false;
  protected resetStickySignatory: boolean = false;
  protected errorMessage: string = '';
  protected errorType: string = '';

  abstract echo(): Observable<string>;

  setKeyStore(keyStore: KeyStore | null) {
    this.keyStore = keyStore;
    this.resetStickySignatory = true;
  }

  setStickySignatory(sticky: boolean) {
    if (this.stickySignatory && !sticky) {
      this.resetStickySignatory = true;
		}

		this.stickySignatory = sticky;
  }

  getErrorMessage(): string {
    return this.errorMessage;
  }

  getErrorType(): string {
    return this.errorType;
  }
}

