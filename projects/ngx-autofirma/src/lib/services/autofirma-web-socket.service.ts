// MODULE DEPENDENCIES
// - import module dependencies

// -- angular dependencies --
import { Injectable,
         OnDestroy        } from '@angular/core';

// -- rxjs dependencies --
import { Observable,
         retry,
         Subject,
         Subscription     } from 'rxjs';
import { webSocket,
         WebSocketSubject } from 'rxjs/webSocket';

// -- ngx-autofirma dependencies --
import { AutofirmaService } from './autofirma.service';
import   * as AfirmaUtils   from '../utils/autofirma.utils';



const AUTOFIRMA_CONNECTION_RETRIES = 15
const AUTOFIRMA_LAUNCHING_TIME = 2000;
const AUTOFIRMA_WEB_SOCKET_ENDPOINT = 'wss://127.0.0.1:63117';
const AUTOFIRMA_WEB_SOCKET_PROTOCOL_VERSION = 3;



@Injectable({
  providedIn: 'root'
})
export class AutofirmaWebSocketService extends AutofirmaService implements OnDestroy {
  private socket$: WebSocketSubject<any> | null = null;
  private socketSubscription: Subscription | null = null;
  private sessionID: string | null = null;

  constructor() {
    // call parent
    super();

    // create web socket connection to Autofirma
    this.socket$ = webSocket({
      url: AUTOFIRMA_WEB_SOCKET_ENDPOINT,
      openObserver: {
        next: (): void => {
          console.debug(`[AutofirmaWebSocketService] Connection to ${ AUTOFIRMA_WEB_SOCKET_ENDPOINT } established.`);
        }
      },
      closeObserver: {
        next: (closeEvent: CloseEvent): void => {
          console.debug(`[AutofirmaWebSocketService] WebSocket has been closed.`);

          if (!closeEvent.wasClean) {
            console.debug(`[AutofirmaWebSocketService] Trying to reconnect with Autofirma after unexpected disconnection.`);
            this.openNativeApp();
          }
        }
      },
      serializer: (value: string): string => value,
      deserializer: (messageEvent: MessageEvent<string>): string => messageEvent.data
    });

    // create service subscription to force socket connection to Autofirma
    this.socketSubscription = this.socket$.pipe(
      retry({
        count: AUTOFIRMA_CONNECTION_RETRIES,
        delay: AUTOFIRMA_LAUNCHING_TIME,
      })
    ).subscribe({
      error: error => console.error(error)
    });
  }

  ngOnDestroy(): void {
    // close service subscription
    this.socketSubscription?.unsubscribe?.();

    // close web socket connection
    this.socket$?.complete?.();
    this.socket$ = null;
  }

  private openNativeApp(): void {
    console.debug(`[AutofirmaWebSocketService] Opening autofirma native application.`);
    this.sessionID = AfirmaUtils.generateNewIdSession();
    document.location = `afirma://websocket?v=${ AUTOFIRMA_WEB_SOCKET_PROTOCOL_VERSION }&idsession=${ this.sessionID }`;
    console.debug(`[AutofirmaWebSocketService] Autofirma native application opened with sessionID '${ this.sessionID }'.`);
  }

  echo(): Observable<string> {
    // create a subject for echo response
    const echoResponseSubject = new Subject<string>();

    // subscribe to web socket to wait echo response from autofirma
    const echoRequestSubscription = this.socket$?.pipe(
      retry({
        count: AUTOFIRMA_CONNECTION_RETRIES,
        delay: AUTOFIRMA_LAUNCHING_TIME,
      }),
    ).subscribe({
      next: message => {
        // notify echo response
        echoResponseSubject.next(message);
        echoResponseSubject.complete();

        // close web socket subscription
        echoRequestSubscription?.unsubscribe();
      }
    });

    // send echo message to autofirma
    this.socket$?.next(`echo=-idsession=${ this.sessionID }@EOF`);

    return echoResponseSubject.asObservable();
  }
}
