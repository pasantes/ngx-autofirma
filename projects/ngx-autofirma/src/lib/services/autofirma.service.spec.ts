// MODULE DEPENDENCIES
// - import module dependencies

// -- ngx-autofirma module dependencies --
import { autofirmaServiceFactory } from './autofirma.provider';
import { AutofirmaService        } from "./autofirma.service";



// UNIT TESTS DEFINITION
// - set of unit test for Autofirma service
describe('Unit tests: Autofirma Service', () => {
  let autofirma: AutofirmaService;

  // - initialize test
  beforeEach(() => { autofirma = autofirmaServiceFactory(); });
});
