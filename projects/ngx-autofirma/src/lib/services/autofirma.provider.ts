// MODULE DEPENDENCIES
// - import module dependencies

// -- ngx-autofirma dependencies --
import { AutofirmaService          } from '../services/autofirma.service';
import { AutofirmaWebSocketService } from '../services/autofirma-web-socket.service';
import { KeyStore                  } from '../types/keystore';
import   * as Platform               from '../utils/platform.utils';



export function autofirmaServiceFactory(): AutofirmaService {
  let autofirmaService: AutofirmaService;

  // initialize afirma service for current platform
  if (Platform.isWebSocketsSupported() && !Platform.isInternetExplorer()) {
    autofirmaService = new AutofirmaWebSocketService();
    console.debug('autofirmaServiceFactory: AutofirmaWebSocketService created.');
  } else {
    throw new Error('Only web socket connection is supported currently.');
  }

  // set default key store
  autofirmaService.setKeyStore(Platform.isFirefox() ? KeyStore.Mozilla : null);

  return autofirmaService;
}
