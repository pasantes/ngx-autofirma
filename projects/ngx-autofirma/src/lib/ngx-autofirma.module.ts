// MODULE DEPENDENCIES
// - import module dependencies

// -- angular dependencies --
import { NgModule                } from '@angular/core';

// -- ngx-autofirma module dependencies --
import { autofirmaServiceFactory } from './services/autofirma.provider';
import { AutofirmaService        } from './services/autofirma.service';



@NgModule({
  providers: [{ provide: AutofirmaService, useFactory: autofirmaServiceFactory }]
})
export class NgxAutofirmaModule {}
