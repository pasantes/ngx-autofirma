export enum KeyStore {
  Windows   = 'WINDOWS',
  Apple     = 'APPLE',
  PKCS12    = 'PKCS12',
  PKCS11    = 'PKCS11',
  Mozilla   = 'MOZ_UNI',
  SharedNSS = 'SHARED_NSS',
  Java      = 'JAVA',
  JCEKS     = 'JCEKS',
  JavaCE    = 'JAVACE',
  DNIeJava  = 'DNIEJAVA'
}
