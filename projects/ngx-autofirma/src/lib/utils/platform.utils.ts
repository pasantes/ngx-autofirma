/**
 * Returns true if the operating system is Android.
 * We don't know why autoscript include Kindle devices as Android.
 * @returns { boolean }
 */
export function isAndroid(): boolean {
  return (
    navigator.userAgent.toUpperCase().indexOf("ANDROID")     != -1 ||
    navigator.appVersion.toUpperCase().indexOf("ANDROID")    != -1 ||
    // Para la deteccion de los Kindle Fire
		navigator.userAgent.toUpperCase().indexOf("SILK/")       != -1 ||
		navigator.userAgent.toUpperCase().indexOf("KFJWI")       != -1 ||
		navigator.userAgent.toUpperCase().indexOf("KFJWA")       != -1 ||
		navigator.userAgent.toUpperCase().indexOf("KFTT")        != -1 ||
		navigator.userAgent.toUpperCase().indexOf("KFOT")        != -1 ||
		navigator.userAgent.toUpperCase().indexOf("KINDLE FIRE") != -1
  );
}

/**
 * Returns true if the operating system is iOS
 * @returns { boolean }
 */
export function isIOS(): boolean {
  return (
    navigator.userAgent.toUpperCase().indexOf("IPAD")   != -1 ||
    navigator.userAgent.toUpperCase().indexOf("IPOD")   != -1 ||
    navigator.userAgent.toUpperCase().indexOf("IPHONE") != -1 ||
   // En iOS 13, Safari tiene el mismo userAgent que el Safari de macOS,
   // asi que lo distinguimos por los puntos de presion admitidos
    navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1
  );
}

/**
 * Returns true if the browser is Internet Explorer
 * @returns { boolean }
 */
export function isInternetExplorer(): boolean {
  return (
    !!(navigator.userAgent.match(/MSIE/))                                          ||  /* Internet Explorer 10 o inferior */
    !!(navigator.userAgent.match(/Trident/) && navigator.userAgent.match(/rv:11/)) ||  /* Internet Explorer 11 (Opcion 1) */
    !!(navigator.userAgent.match(/Trident.*rv[ :]*11\./))                              /* Internet Explorer 11 (Opcion 2) */
  );
}

/**
 * Returns true if the browser is Internet Explorer 10 or lower
 * @returns { boolean }
 */
export function isInternetExplorer10orLower(): boolean {
  return !!(navigator.userAgent.match(/MSIE/));
}

/**
 * Returns true if the browser is Mozilla Firefox
 * @returns { boolean }
 */
export function isFirefox(): boolean {
  return navigator.userAgent.toUpperCase().indexOf("FIREFOX") != -1;
}

/**
 * Returns true if the browser is Google Chrome
 * @returns { boolean }
 */
export function isChrome(): boolean {
  return (
    navigator.userAgent.toUpperCase().indexOf("CHROME/")  != -1 ||
    navigator.userAgent.toUpperCase().indexOf("CHROMIUM") != -1
  );
}

/**
 * Returns true if the browser supports web sockets
 * @returns { boolean }
 */
export function isWebSocketsSupported(): boolean {
  return 'WebSocket' in window || 'MozWebSocket' in window;
}
