// MODULE DEPENDENCIES
// - import module dependencies

// -- ngx-autofirma module dependencies --
import { generateCipherKey,
         generateNewIdSession } from "./autofirma.utils";



// UNIT TESTS DEFINITION
// - set of unit test for Autofirma utils
describe('Unit tests: Autofirma Utils', () => {
  it('generateNewIdSession should generate a valid session id', () => {
    expect(generateNewIdSession()).toMatch(/^[0-9a-zA-Z]{20}$/);
  });

  it('generateCipherKey should be a valid cipher key', () => {
    expect(generateCipherKey()).toMatch(/^\d{8}$/);
  });
});
