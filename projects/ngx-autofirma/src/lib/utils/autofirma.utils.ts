const ID_LENGTH = 20;
const MAX_NUMBER = 2147483648;
const VALID_CHARS_TO_ID = '1234567890abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

let seed: number;

/**
 * Get a random number with a homogeneous distribution.
 * @returns { number } Random number with homogeneous distribution.
 */
function rnd(): number {
  if (!seed) {
    seed = new Date().getMilliseconds() * 1000 * Math.random();
  }

  seed = (seed * 9301 + 49297) % 233280;
	return seed / 233280;
}

/**
 * Fill in a number with zeros to the left.
 * @param { number } number Number to be filled.
 * @param { number } width  Desired number length.
 * @returns { string } Number padded with zeros until the desired length is reached.
 */
function zeroFill(number: number, width: number): string {
  width -= number.toString().length;
  if (width > 0) {
    return new Array(width + (/\./.test(number.toString()) ? 2 : 1)).join('0')
    + number;
  }
  return number + "";
}

/**
 * Get a session identifier.
 * @returns { string } Session identifier.
 */
export function generateNewIdSession(): string {
  let random = '';
  let randomInts;

  if (typeof window.crypto != "undefined" && typeof window.crypto.getRandomValues != "undefined") {
    randomInts = new Uint32Array(ID_LENGTH);
    window.crypto.getRandomValues(randomInts);
  }
  else {
    randomInts = new Array(ID_LENGTH);
    for (let i = 0; i < ID_LENGTH; i++) {
      randomInts[i] = rnd() * MAX_NUMBER;
    }
  }

  for (let i = 0; i < ID_LENGTH; i++) {
    random += VALID_CHARS_TO_ID.charAt(Math.floor(randomInts[i] % VALID_CHARS_TO_ID.length));
  }

  return random;
}

/**
 * Get a cipher key.
 * @returns { string } Cipher key.
 */
export function generateCipherKey(): string {
  let random;
  if (typeof window.crypto != "undefined" && typeof window.crypto.getRandomValues != "undefined") {
    var randomInts = new Uint32Array(1);
    window.crypto.getRandomValues(randomInts);
    random = zeroFill(randomInts[0] % 100000000, 8);
  }
  else {
    random = zeroFill(Math.floor(((rnd() * MAX_NUMBER) + 1) % 100000000), 8);
  }

  return random;
}
